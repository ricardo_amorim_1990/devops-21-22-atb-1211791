CA4 Parte 1

Para este projeto vamos usar o Docker para criar imagens e conteiners.

A primeira parte será dividida em duas partes. Uma primeira iremos clonar a aplicação para um conteiner e correr o Server. Na segunda parte vamos correr a aplicação na nossa maquina local a partir do conteiner.

Em primeiro realizei o download do docker
Depois de instalado criei um dockerfile na pasta v1.
Este ficheiro é constituido por :

FROM ubuntu - para correr a aplicação no ubunto

RUN apt-get update -y - para instalar componentes esquecidos do ubunto

RUN apt-get install -y openjdk-8-jdk-headless- para correr o java da app
RUN apt-get install -y git - para conseguirmos clonar a app

RUN git clone https://ricardo_amorim_1990@bitbucket.org/ricardo_amorim_1990/devops-21-22-atb-1211791.git - para realizar o clone do meu repositorio

WORKDIR devops-21-22-atb-1211791/CA2/Part1 - o path onde tenho o meu dockerfile

RUN chmod u+x gradlew - para dar permissoes
RUN ./gradlew clean build - para realizar o build da app

EXPOSE 59001  - correr a app na porta 59001

ENTRYPOINT ./gradlew runServer

Para criar imagens e conteiners corri a seguinte linha de comandos:

docker build runserver .

docker run -p 59001:59001 --name runserver -d runserver

Depois de correr o conteiner fiz um push para o docker hub da minha conta

docker login

docker tag runserver:latest 1211791/runserver:CA4-v1 
docker push 1211791/runserver:CA4-v1

Na segunda parte criei um novo ficheiro para onde copiei o ficheiro jar e criei um novo docker file.

FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-11-jdk-headless

COPY basic_demo-0.1.0.jar basic_demo-0.1.0.jar - onde reallizo a copia do ficheiro jar para o conteiner.

EXPOSE 59001

ENTRYPOINT java -cp basic_demo-0.1.0.jar

Depois de testado, realizei o push a conta do docker hub.

docker login

docker tag runserver:latest 1211791/runserver:CA4-v2
docker push 1211791/runserver:CA4-v2

Fim do CA4 parte 1.