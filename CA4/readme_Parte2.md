CA4 Parte 2

Nesta segunda parte foi criado um ficheiro docker compose para mais tarde executar dois ficheiros diferentes.

Recorri ao ficheiro docker compose do projeto docker compose spring tut demo para ter como exemplo para criar o meu ficheiro.

O meu conteiner db era composto por :

FROM ubuntu:18.04

RUN apt-get update && \
apt-get install -y openjdk-8-jdk-headless && \
apt-get install unzip -y && \
apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

O meu conteiner web era composto por :

FROM tomcat:9.0-jdk11

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://ricardo_amorim_1990@bitbucket.org/atb/tut-basic-gradle-docker.git
WORKDIR /tmp/build/tut-basic-gradle-docker

#RUN git clone https://ricardo_amorim_1990@bitbucket.org/ricardo_amorim_1990/devops-21-22-atb-1211791.git

#WORKDIR /tmp/build/devops-21-22-atb-1211791/CA4/Parte2/react-and-spring-data-rest-basic

RUN chmod u+x gradlew

RUN ./gradlew clean build

RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/


EXPOSE 8080


Aqui tive alguns problema com a versão do tomcat onde tive de experimentar diferentes versões.

Depois disto clonei o repositorio do professor pois estava a ter problemas com o meu repositiorio do CA3.

Na linha de comandos escrevi:

docker-compose

E de seguida coloquei o nome das imagens que queria enviar para o docker hub atraves dos comandos:

tag docker (...) db:latest ..../...db:latest

docker push (...) db:latest ..../...db:latest

tag docker (...) web:latest ..../...web:latest

docker push (...) web:latest ..../...web:latest

Por fim realizei a copia do conteiner db para o volume.

Para tal na linha de comandos escrevi:

docker-compose exec db sh

Dentro do conteiner usado:

cp /usr/src/app/jpadb.mv.db /usr/src/databackup

Enviando assim o ficheiro para pasta de dados da minha maquina local.
