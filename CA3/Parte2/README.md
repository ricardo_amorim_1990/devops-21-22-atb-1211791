CA3 - Parte 2

Virtualização - Vagrant


1. Verificação do Repositorio

Ir ate https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ e ver o ficheiro Readme.

2. Copia do Vangrantfile

Foi feita uma copia do ficheiro Vangrantfile para a maquina local.

3. Copia do CA2 Parte 2 e alterações

3.1 Ir a outro repositorio (https://bitbucket.org/atb/tut-basic-gradle/src/master/) e verificar as alterações necessárias a serem implementadas no meu projeto.

3.2 Ir ao ficheiro build.gradle e adicionar o build war files.

3.3 Adicionar o id 'war' na secção dos plugins

plugins {
	id 'org.springframework.boot' version '2.6.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id "org.siouan.frontend-jdk8" version "6.0.0"
	id "war"
}

3.4 Adicionar o providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat' na secção das dependências 

dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-data-rest'
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	runtimeOnly 'com.h2database:h2'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	implementation 'org.testng:testng:7.1.0'
	implementation 'org.junit.jupiter:junit-jupiter:5.7.0'

	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}

3.5 Criar o ServletInitializer.java com o seguinte código

package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}

3.6 Adicionar suporte para a consola h2 e configurações para a base de dados h2

No application.properties adicionar :

server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true

3.7 Modificar o path 

No ficheiro app.js adicionar :

componentDidMount() { // <2>
		client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}

3.8 Ir ao index.html e remover o "antes" do "main.css" da linha 6:

<link rel="stylesheet" href="main.css" />

4. Fazer Update do repositorio remoto

4.1 Commit e push das alterações feitas

6 Inicializar as VM

6.1 Na linha de comandos usar vagrant up para inicializar 
6.2 Build feito com sucesso

7. Verificação do FrontEnd e H2

7.1 Tendo as VM's a correr no browser escrever http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

7.2 Escrever http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console para ver a base de dados 

8. Processo concluido