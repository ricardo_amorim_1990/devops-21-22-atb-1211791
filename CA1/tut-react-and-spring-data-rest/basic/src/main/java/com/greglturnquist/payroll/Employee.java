/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private int jobYears;
	private String email;

	protected Employee() {}

	public Employee(String firstName, String lastName, String description,int jobYears,String email) {
		{
			if (validateFirstName(firstName) && validateLasteName(lastName) && validateDescription(description) && validateJobYears(jobYears)
					&& validateEmail(email)) {
				this.firstName = firstName;
				this.lastName = lastName;
				this.description = description;
				this.jobYears = jobYears;
				this.email = email;
			} else {
				throw new IllegalArgumentException("Inputs is not valid!");
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
				Objects.equals(jobYears, employee.jobYears) &&
				Objects.equals(email,employee.email);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description,jobYears,email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getJobYears(){ return jobYears; }

	public void setJobYears(int jobYears) {this.jobYears = jobYears;}

	public String getEmail() { return email;}

	public void setEmail(String email) {this.email = email;}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
				", email='" + email + '\'' +
			'}';
	}

	public boolean validateFirstName(String firstName) {
		boolean valid;

		valid = firstName != null && !firstName.isEmpty() && firstName.trim().length() != 0;
		return valid;
	}


	public boolean validateLasteName(String lastName) {

		boolean valid;

		valid = lastName != null && !lastName.isEmpty() && lastName.trim().length() != 0;
		return valid;
	}


	public boolean validateDescription(String description) {

		boolean valid;

		valid = description != null && !description.isEmpty() && description.trim().length() != 0;
		return valid;
	}

	public boolean validateJobYears(int jobYears) {

		boolean valid;

		valid = jobYears >= 0;
		return valid;
	}

	public boolean validateEmail(final String email) {

		boolean valid;

		if (email != null || email.length() != 0 || email.trim().length() == 0) {
			valid = false;
		}else{
			String emailRegex = "[A-Z0-9a-z._%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
			Pattern pat = Pattern.compile(emailRegex);
			valid = pat.matcher(email).matches();
		}
		return valid;}

	}


// end::code[]
