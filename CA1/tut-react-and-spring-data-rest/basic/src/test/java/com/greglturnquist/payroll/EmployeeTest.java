package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test

    void validateFirstNameIsEmpty(){

        //Arrange
        String firstName ="";
        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee(firstName,"Marques","Era Medico",4,"joao@gmail"));

    }

    @Test
    void validateLastNameIsEmpty(){

        //Arrange
        String lastName ="";
        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee("Joao",lastName,"Era Medico",4,"joao@gmail"));

    }

    @Test
    void validateDescriptionIsEmpty(){

        //Arrange
        String description ="";
        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee("Joao","Marques",description,4,"joao@gmail"));

    }

    @Test
    void validateJobYearsIsNegative(){

        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee("Joao","Marques","Era medico",-1,"joao@gmail"));

    }
    @Test
    void validateEmail() {
        String email="";

        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee("Joao","Marques","Era medico",-1,email));
    }
    @Test
    void validateEmailWrong() {
        String email="jonasgmail.com";

        //Act
        assertThrows(IllegalArgumentException.class, () -> new Employee("Joao","Marques","Era medico",-1,email));
    }
}



